package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

//NEXT STEPS
//create server
//settup docker
//get server IP
//connect server to gitlab
//->LAUNCH

//USEFUL STUFF:
//docker run --name mongoserv -v mongodata:/data/db -p 27017:27017 -d mongo:latest

//Token ID
var Token string

//IP for Mongo
var IP string

type meme struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	Keyphrase string             `bson:"keyphrase,omitempty"`
	Link      string             `bson:"link,omitempty"`
}

func init() {
	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.StringVar(&IP, "i", "", "IP")
	flag.Parse()
}

func main() {
	//flag check
	if Token == "" {
		fmt.Println("No Token. Please use: -t <Bot Token>")
		return
	}
	if IP == "" {
		fmt.Println("No IP. Please use: -i <IP>")
		return
	}

	//connect to discord bot
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("Error: Session did not launch", err)
		return
	}
	dg.AddHandler(messageCreate)
	dg.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildMessages)
	err = dg.Open()
	if err != nil {
		fmt.Println("Token don't work", err)
		return
	}
	fmt.Println("Bot is ready")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	//close parameters
	dg.Close()
	fmt.Println("\nGoodbye")

}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Connect DB
	uri := fmt.Sprintf("mongodb://UserAdmin:admin@%s", IP)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		fmt.Println("Error connecting to Database1")
		panic(err)
	}
	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			fmt.Println("Error connecting to Database2")
			panic(err)
		}
	}()

	// Ping DB
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		fmt.Println("Error connecting to Database3")
		panic(err)
	}
	fmt.Println("Successfully connected and pinged.")

	//create/reference DB
	memeDb := client.Database("memeDb")
	memeCl := memeDb.Collection("memeCl")

	//create index for text search
	mod := mongo.IndexModel{
		Keys: bson.M{
			"keyphrase": "text",
		}, Options: nil,
	}
	_, err = memeCl.Indexes().CreateOne(ctx, mod)
	if err != nil {
		fmt.Println("Error connecting to Database4")
		panic(err)
	}

	//if statements
	if m.Author.ID == s.State.User.ID {
		return
	}
	if strings.HasPrefix(m.Content, "!add") {
		addMeme(ctx, s, m, memeCl)
	}
	if strings.HasPrefix(m.Content, "!remove") {
		removeMeme(ctx, s, m, memeCl)
	}

	//auto response
	var msgA = strings.Split(m.Content, " ")
	for _, value := range msgA {
		var answer []meme
		cursor, err := memeCl.Find(ctx, bson.M{"$text": bson.M{"$search": value}})
		if err != nil {
			continue
		}
		if err = cursor.All(ctx, &answer); err != nil {
			continue
		}
		for _, obj := range answer {
			if strings.Contains(m.Content, obj.Keyphrase) {
				s.ChannelMessageSend(m.ChannelID, obj.Link)
				return
			}
		}
	}
}

func addMeme(ctx context.Context, s *discordgo.Session, m *discordgo.MessageCreate, memeCl *mongo.Collection) {
	//split message into array
	var msgA = strings.Split(m.Content, " ")
	//lenth check
	if len(msgA) < 3 {
		s.ChannelMessageSend(m.ChannelID, "Error Please use: !add <keyphrase or keyword> <image link>")
		return
	}

	//var set
	l := len(msgA)
	keyphraseValues := msgA[1 : l-1]
	keyphrase := strings.Join(keyphraseValues, " ")
	link := msgA[l-1]
	obj := meme{
		Keyphrase: keyphrase,
		Link:      link,
	}

	//add to DB
	_, err := memeCl.InsertOne(ctx, obj)
	if err != nil {
		s.ChannelMessageSend(m.ChannelID, "Error adding to Database5")
	}
	s.ChannelMessageSend(m.ChannelID, "Added to Database!")
	return
}

func removeMeme(ctx context.Context, s *discordgo.Session, m *discordgo.MessageCreate, memeCl *mongo.Collection) {
	//split message into array
	var msgA = strings.Split(m.Content, " ")
	l := len(msgA)
	deleteKeyphrase := strings.Join(msgA[1:l], " ")

	//lenth check
	if l < 2 {
		s.ChannelMessageSend(m.ChannelID, "Error Please use: !remove <keyword/keyphrase>")
		return
	}

	//one word keyphrase remove
	if l == 2 {
		deleteRes, err := memeCl.DeleteOne(ctx, bson.M{"keyphrase": msgA[1]})
		fmt.Println(deleteRes.DeletedCount)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Error removing from Database6")
		}
		if deleteRes.DeletedCount == 1 {
			s.ChannelMessageSend(m.ChannelID, "Removed from Database!")
		}
	}

	//keyphrase remove
	if l > 2 {
		deleteRes, err := memeCl.DeleteOne(ctx, bson.M{"keyphrase": deleteKeyphrase})
		fmt.Println(deleteRes.DeletedCount)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Error removing from Database7")
		}
		if deleteRes.DeletedCount == 1 {
			s.ChannelMessageSend(m.ChannelID, "Removed from Database!")

		}
	}
}
