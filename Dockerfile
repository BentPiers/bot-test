FROM golang:1.15-alpine

ENV IP=mongo

RUN mkdir /app
ADD . /app
WORKDIR /app

RUN apk update
RUN apk add git
RUN go get -u github.com/bwmarrin/discordgo
RUN go get -u go.mongodb.org/mongo-driver/bson
RUN go get -u go.mongodb.org/mongo-driver/bson/primitive
RUN go get -u go.mongodb.org/mongo-driver/mongo
RUN go get -u go.mongodb.org/mongo-driver/mongo/options
RUN go get -u go.mongodb.org/mongo-driver/mongo/readpref

CMD go run main.go -t Nzc2MjQ4MDQ3NTAzMDE1OTc2.X6yHVA.Nod6Xf0zjpaoYg2zejLBPadDvvg -i $IP
